import { Mongo } from 'meteor/mongo';
 
export const Tasks = new Mongo.Collection('tasks');
export const Zpt = new Mongo.Collection('zpt');
export const Bars = new Mongo.Collection('bars');
export const Points = new Mongo.Collection('points');
export const DecamTiles = new Mongo.Collection('decam_tiles');

