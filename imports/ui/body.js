import { Template } from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';

import { Tasks } from '../api/tasks.js';
import { Zpt } from '../api/tasks.js';
import { Bars } from '../api/tasks.js';

import './plots/tiles.js';
import './plots/barChart.js';
import './plots/scatterPlot.js';
import './task.js';
import './body.html';
 
Template.body.onCreated(function bodyOnCreated() {
  this.state = new ReactiveDict();
});

Template.body.helpers({
  //func 
  tasks() {
  	const instance = Template.instance();
    if (instance.state.get('hideCompleted')) {
      // If hide completed is checked, filter tasks
      return Tasks.find({ checked: { $ne: true } }, { sort: { createdAt: -1 } });
    }
    // Otherwise, return all of the tasks
    return Tasks.find({}, { sort: { createdAt: -1 } });
  },
  //2nd func
  incompleteCount() {
    return Tasks.find({ checked: { $ne: true } }).count();
  },
  ccdCount() {
    return Zpt.find().count();
  },
  barsCount() {
    return Bars.find().count();
  },
});

Template.body.events({
  'submit .new-task'(event) {
    // Prevent default browser form submit
    console.log(event)
    event.preventDefault();
 
    // Get value from form element
    const target = event.target;
    const text = target.text.value;
 
    // Insert a task into the collection
    Tasks.insert({
      text,
      createdAt: new Date(), // current time
    });
 
    // Clear form
    target.text.value = '';
  },
  // new event
  'change .hide-completed input'(event, instance) {
    instance.state.set('hideCompleted', event.target.checked);
  },
});

