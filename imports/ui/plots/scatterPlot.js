import { Session } from 'meteor/session';
//import d3 from 'd3';

import '/client/stylesheets/plots.css'
import './scatterPlot.html';

// var Points = new Meteor.Collection(null);
// import { Points } from '../api/tasks.js';
import { Zpt } from '../../api/tasks.js';

if(Zpt.find({}).count() === 0){
	for(i = 0; i < 50; i++)
		Zpt.insert({
			x:Math.floor(Math.random() * 1000),
			y:Math.floor(Math.random() * 1000)
		});
}

Template.scatterPlot.events({
	'click #randomize':function(){
		//loop through bars
		Zpt.find({}).forEach(function(bar){
			//update the value of the bar
			Zpt.update({_id:bar._id},{$set:{
				x:Math.floor(Math.random() * 1000),
				y:Math.floor(Math.random() * 1000)
			}});
		});
	}
});

Template.scatterPlot.onRendered(function() {
// Template.scatterPlot.rendered = function(){
	//Width and height
	var w = 500;
	var h = 300;
	var padding = 60;
	
	//Create scale functions
	var xScale = d3.scale.linear()
						 .range([padding, w - padding * 2]);

	var yScale = d3.scale.linear()
						 .range([h - padding, padding]);

	//Define X axis
	var xAxis = d3.svg.axis()
					.scale(xScale)
					.orient("bottom")
					.ticks(5);

	//Define Y axis
	var yAxis = d3.svg.axis()
					.scale(yScale)
					.orient("left")
					.ticks(5);

	//Create SVG element
	var svg = d3.select("#scatterPlot")
				.attr("width", w)
				.attr("height", h);

	//Define key function, to be used when binding data
	var key = function(d) {
		return d._id;
	};

	//Create X axis
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + (h - padding) + ")");
	
	// x-label
	svg.append("text")
		.attr("x", w / 2 )
        .attr("y",  h - padding/3)      
        .style("text-anchor", "middle")
        .text("zpt");

	//Create Y axis
	svg.append("g")
		.attr("class", "y axis")
		.attr("transform", "translate(" + padding + ",0)");

	// y-label
	svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", w/2)
        .attr("x",h)
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text("rarms");

	Deps.autorun(function(){
		var dataset = Zpt.find({},{zpt:1,rarms:1}).fetch();
		
		//Update scale domains
		xScale.domain([d3.min(dataset, function(d) { return d.zpt; }), d3.max(dataset, function(d) { return d.zpt; })]);
		yScale.domain([d3.min(dataset, function(d) { return d.rarms; }), d3.max(dataset, function(d) { return d.rarms; })]);

		//Update X axis
		svg.select(".x.axis")
			.transition()
			.duration(1000)
			.call(xAxis);
		
		//Update Y axis
		svg.select(".y.axis")
			.transition()
			.duration(1000)
			.call(yAxis);
		

		var circles = svg
			.selectAll("circle")
			.data(dataset, key);

		//Create
		circles
			.enter()
			.append("circle")
			.attr("cx", function(d) {
				return xScale(d.zpt);
			})
			.attr("cy", function(d) {
				return yScale(d.rarms);
			})
			.attr("r", 10);

		//Update
		circles
			.transition()
			.duration(1000)
			.attr("cx", function(d) {
				return xScale(d.zpt);
			})
			.attr("cy", function(d) {
				return yScale(d.rarms);
			});

		//Remove
		circles
			.exit()
			.remove();
	});
});